import 'package:empresas_flutter/app/modules/home/domain/usecases/get_companies_usecase.dart';
import 'package:empresas_flutter/app/modules/home/external/dio/dio_get_companies_datasource.dart';
import 'package:empresas_flutter/app/modules/home/infra/repositories/get_companies_repository.dart';

class GetCompaniesFactory {
  GetCompaniesUsecase getCompaniesUseCaseInstance() => GetCompaniesUsecase(
        repository: GetCompaniesRepository(
          datasource: GetCompaniesDataSource(),
        ),
      );
}
