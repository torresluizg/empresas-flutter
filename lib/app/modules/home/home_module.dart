import 'package:empresas_flutter/app/modules/company_detail/company_detail_page.dart';
import 'package:empresas_flutter/app/modules/company_detail/company_detail_store.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../home/home_store.dart';

import 'home_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => HomeStore()),
    Bind.lazySingleton((i) => CompanyDetailStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => const HomePage()),
    ChildRoute('company-detail',
        child: (_, args) =>
            CompanyDetailPage(company: args.data as CompanyModel)),
  ];
}
