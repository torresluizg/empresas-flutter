import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/home/domain/repositories/companies_repository_interface.dart';
import 'package:empresas_flutter/app/modules/home/infra/repositories/datasources/get_companies_datasource.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';

class GetCompaniesRepository implements IGetCompaniesRepository {
  final IGetCompaniesDataSource datasource;

  GetCompaniesRepository({
    required this.datasource,
  });

  @override
  Future<Either<AuthException, List<CompanyModel>>> getCompanies(
          String path) async =>
      // ignore: unnecessary_await_in_return
      await datasource.getCompanies(path);
}
