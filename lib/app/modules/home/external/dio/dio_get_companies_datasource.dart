import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/modules/home/infra/repositories/datasources/get_companies_datasource.dart';
import 'package:empresas_flutter/app/shared/backend/backend_routes.dart';
import 'package:empresas_flutter/app/shared/backend/backend_service.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';
import 'package:flutter_modular/flutter_modular.dart';

class GetCompaniesDataSource implements IGetCompaniesDataSource {
  @override
  Future<Either<AuthException, List<CompanyModel>>> getCompanies(
      String path) async {
    final backendService = Modular.get<BackendService>();
    try {
      final Response backendResponse = await backendService.io.get(
        '${BackendRoutes.apiVersion}$path',
      );
      final List<CompanyModel> tempList = [];
      final List companies = backendResponse.data['enterprises'] as List;
      // ignore: cascade_invocations
      companies.forEach((element) {
        final CompanyModel temp =
            CompanyModel.fromJson(element as Map<String, dynamic>);
        tempList.add(temp);
      });
      return Right(tempList);
    } catch (e) {
      return Left(AuthException(message: '$e'));
    }
  }
}
