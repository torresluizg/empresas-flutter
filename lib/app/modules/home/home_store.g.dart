// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeStore on HomeStoreBase, Store {
  final _$searchControllerAtom = Atom(name: 'HomeStoreBase.searchController');

  @override
  TextEditingController get searchController {
    _$searchControllerAtom.reportRead();
    return super.searchController;
  }

  @override
  set searchController(TextEditingController value) {
    _$searchControllerAtom.reportWrite(value, super.searchController, () {
      super.searchController = value;
    });
  }

  final _$isSearchControllerTextEmptyAtom =
      Atom(name: 'HomeStoreBase.isSearchControllerTextEmpty');

  @override
  bool get isSearchControllerTextEmpty {
    _$isSearchControllerTextEmptyAtom.reportRead();
    return super.isSearchControllerTextEmpty;
  }

  @override
  set isSearchControllerTextEmpty(bool value) {
    _$isSearchControllerTextEmptyAtom
        .reportWrite(value, super.isSearchControllerTextEmpty, () {
      super.isSearchControllerTextEmpty = value;
    });
  }

  final _$companyModelListAtom = Atom(name: 'HomeStoreBase.companyModelList');

  @override
  List<CompanyModel> get companyModelList {
    _$companyModelListAtom.reportRead();
    return super.companyModelList;
  }

  @override
  set companyModelList(List<CompanyModel> value) {
    _$companyModelListAtom.reportWrite(value, super.companyModelList, () {
      super.companyModelList = value;
    });
  }

  final _$companyModelListFilteredAtom =
      Atom(name: 'HomeStoreBase.companyModelListFiltered');

  @override
  List<CompanyModel> get companyModelListFiltered {
    _$companyModelListFilteredAtom.reportRead();
    return super.companyModelListFiltered;
  }

  @override
  set companyModelListFiltered(List<CompanyModel> value) {
    _$companyModelListFilteredAtom
        .reportWrite(value, super.companyModelListFiltered, () {
      super.companyModelListFiltered = value;
    });
  }

  final _$getCompaniesAsyncAction = AsyncAction('HomeStoreBase.getCompanies');

  @override
  Future<void> getCompanies() {
    return _$getCompaniesAsyncAction.run(() => super.getCompanies());
  }

  @override
  String toString() {
    return '''
searchController: ${searchController},
isSearchControllerTextEmpty: ${isSearchControllerTextEmpty},
companyModelList: ${companyModelList},
companyModelListFiltered: ${companyModelListFiltered}
    ''';
  }
}
