import 'package:empresas_flutter/app/shared/models/company_model.dart';

class ListCompanies {
  final List<CompanyModel> listOfCompanies;

  ListCompanies({
    required this.listOfCompanies,
  });
}
