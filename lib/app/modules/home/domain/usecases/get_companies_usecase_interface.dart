import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';

// ignore: one_member_abstracts
abstract class IGetCompaniesUsecase {
  Future<Either<AuthException, List<CompanyModel>>> call(String path);
}
