import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/home/domain/repositories/companies_repository_interface.dart';
import 'package:empresas_flutter/app/modules/home/domain/usecases/get_companies_usecase_interface.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';

class GetCompaniesUsecase implements IGetCompaniesUsecase {
  final IGetCompaniesRepository repository;

  GetCompaniesUsecase({
    required this.repository,
  });
  @override
  Future<Either<AuthException, List<CompanyModel>>> call(String path) =>
      repository.getCompanies(path);
}
