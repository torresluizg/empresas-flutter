import 'package:cached_network_image/cached_network_image.dart';
import 'package:empresas_flutter/app/modules/home/home_store.dart';
import 'package:empresas_flutter/app/shared/theme/theme_colors.dart';
import 'package:empresas_flutter/app/shared/widgets/text_field/text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeStore> {
  // ignore: annotate_overrides
  final store = Modular.get<HomeStore>();

  @override
  void initState() {
    store.getCompanies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Stack(
            children: [
              Observer(
                builder: (_) {
                  final bool isSearchEmpty = store.isSearchControllerTextEmpty;
                  if (isSearchEmpty) {
                    return Container(
                      height: 200,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage('assets/images/bg_medium_style.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    );
                  }
                  return Container(
                    height: 100,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/bg_short.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Observer(
                      builder: (_) {
                        final bool isSearchEmpty =
                            store.isSearchControllerTextEmpty;
                        if (isSearchEmpty) {
                          return const SizedBox(height: 150);
                        }
                        return const SizedBox(height: 50);
                      },
                    ),
                    TextFieldWidget(
                      controller: store.searchController,
                      title: 'Pesquise por empresa',
                      hint: 'Digite o nome da empresa',
                      isSearch: true,
                      onChanged: (value) =>
                          store.filteredCompanyList(value as String),
                    ),
                    const SizedBox(height: 20),
                    Observer(builder: (_) {
                      if (store.isSearchControllerTextEmpty ||
                          store.companyModelListFiltered.isEmpty) {
                        return Container();
                      }
                      return Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '${store.companyLengthStylized()} resultados encontrados',
                          style: const TextStyle(
                            color: ThemeColors.grey,
                            fontSize: 18,
                          ),
                        ),
                      );
                    }),
                    const SizedBox(height: 20),
                    Observer(
                      builder: (_) {
                        if (store.isSearchControllerTextEmpty) {
                          return Container();
                        }
                        if (!store.isSearchControllerTextEmpty &&
                            store.companyModelListFiltered.isEmpty) {
                          return const Expanded(
                            child: SizedBox(
                              height: double.infinity,
                              child: Center(
                                child: Text(
                                  'Nenhum resultado encontrado',
                                  style: TextStyle(
                                    color: ThemeColors.grey,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }
                        return Expanded(
                          child: ListView.separated(
                            itemCount: store.companyModelListFiltered.length,
                            itemBuilder: (_, index) => GestureDetector(
                              onTap: () => Modular.to.pushNamed(
                                  'company-detail',
                                  arguments:
                                      store.companyModelListFiltered[index]),
                              child: Container(
                                height: 120,
                                width: double.infinity,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(8),
                                  ),
                                  color: Colors.black,
                                ),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      'https://empresas.ioasys.com.br/${store.companyModelListFiltered[index].photo!}',
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(8),
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => const Center(
                                    child: CircularProgressIndicator(
                                      backgroundColor: Colors.red,
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                ),
                              ),
                            ),
                            separatorBuilder: (_, index) =>
                                const SizedBox(height: 10),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
