import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/home/domain/usecases/get_companies_usecase.dart';
import 'package:empresas_flutter/app/modules/home/factory/get_companies_factory.dart';
import 'package:empresas_flutter/app/shared/backend/backend_routes.dart';
import 'package:empresas_flutter/app/shared/backend/backend_service.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore = HomeStoreBase with _$HomeStore;

abstract class HomeStoreBase with Store {
  final backendService = Modular.get<BackendService>();
  @observable
  TextEditingController searchController = TextEditingController();
  @observable
  bool isSearchControllerTextEmpty = true;
  @observable
  List<CompanyModel> companyModelList = [];
  @observable
  List<CompanyModel> companyModelListFiltered = [];

  @action
  Future<void> getCompanies() async {
    final GetCompaniesFactory factory = GetCompaniesFactory();
    final GetCompaniesUsecase getCompaniesUseCase =
        factory.getCompaniesUseCaseInstance();
    final Either<AuthException, List<CompanyModel>> companiesList =
        await getCompaniesUseCase.call(BackendRoutes.categories);
    companiesList.fold(
      (l) {
        print('error: ${l.message}');
      },
      (r) {
        companyModelList = r;
      },
    );
  }

  void filteredCompanyList(String textToFilter) {
    isSearchControllerTextEmpty = searchController.text.isEmpty ? true : false;

    companyModelListFiltered = [];
    final List<CompanyModel> temp = companyModelList.where((element) {
      if (element.enterpriseName != null) {
        return element.enterpriseName!
            .toLowerCase()
            .contains(textToFilter.toLowerCase());
      }
      return false;
    }).toList();
    companyModelListFiltered = temp;
  }

  String companyLengthStylized() {
    final int length = companyModelListFiltered.length;
    if (length > 9) {
      return length.toString();
    }
    return '0$length';
  }
}
