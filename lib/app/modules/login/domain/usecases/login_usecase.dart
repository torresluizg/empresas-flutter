import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/credential_params.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/app/modules/login/domain/repositories/auth_repository_interfece.dart';
import 'package:empresas_flutter/app/modules/login/domain/usecases/login_usecase_interface.dart';
import 'package:email_validator/email_validator.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';

class LoginUseCase extends ILoginUsecase {
  final IAuthRepository repository;

  LoginUseCase({
    required this.repository,
  });
  @override
  Future<Either<AuthException, LoggedUser>> call(
      CredentialsParams params) async {
    if (!EmailValidator.validate(params.email)) {
      return Left(AuthException(message: 'E-mail inválido'));
    }
    if (params.password.isEmpty) {
      return Left(AuthException(message: 'Senha não pode ser vazia'));
    }

    // ignore: unnecessary_await_in_return
    return await repository.login(params);
  }
}
