import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/credential_params.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';

// ignore: one_member_abstracts
abstract class ILoginUsecase {
  Future<Either<AuthException, LoggedUser>> call(CredentialsParams params);
}
