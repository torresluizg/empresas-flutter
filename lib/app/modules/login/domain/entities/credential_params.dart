class CredentialsParams {
  final String password;
  final String email;

  CredentialsParams({
    required this.password,
    required this.email,
  });
}
