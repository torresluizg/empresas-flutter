import 'package:empresas_flutter/app/shared/models/user_model.dart';

class LoggedUser {
  final UserModel client;

  LoggedUser({
    required this.client,
  });
}
