import 'dart:ui';

import 'package:empresas_flutter/app/shared/theme/theme_colors.dart';
import 'package:empresas_flutter/app/shared/widgets/text_field/text_field_widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:empresas_flutter/app/modules/login/login_store.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, this.title = 'LoginPage'}) : super(key: key);
  final String title;
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final LoginStore store = Modular.get();

  @override
  Widget build(BuildContext context) => Scaffold(
        //backgroundColor: Colors.blue,
        body: Stack(
          children: [
            Observer(
              builder: (_) {
                // ignore: prefer_final_locals
                bool hasText = store.hasText;
                return Container(
                  height: !hasText ? 250 : 180,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: const DecorationImage(
                      image: AssetImage('assets/images/bg_medium.png'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.elliptical(
                          MediaQuery.of(context).size.width,
                          !hasText ? 150 : 100),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Image(
                          image:
                              AssetImage('assets/images/logo_minimalist.png')),
                      const SizedBox(height: 16),
                      // ignore: prefer_if_elements_to_conditional_expressions
                      !hasText
                          ? const Text(
                              'Seja bem vindo ao empresas',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                              ),
                            )
                          : Container(),
                    ],
                  ),
                );
              },
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        const SizedBox(height: 300),
                        TextFieldWidget(
                          controller: store.emailController,
                          title: 'Email',
                          hint: 'Digite seu email aqui',
                          // ignore: avoid_types_on_closure_parameters
                          onChanged: (String text) => store.onChanged(text),
                          containerColor: ThemeColors.white,
                        ),
                        const SizedBox(height: 30),
                        TextFieldWidget(
                          controller: store.passwordController,
                          title: 'Senha',
                          hint: 'Digite sua senha aqui',
                          isPassword: true,
                          containerColor: ThemeColors.white,
                        ),
                        const SizedBox(height: 10),
                        Observer(
                          builder: (_) {
                            if (store.errorText.isEmpty) {
                              return Container();
                            }
                            return Container(
                              alignment: Alignment.centerRight,
                              child: Text(
                                store.errorText,
                                style: const TextStyle(
                                  color: ThemeColors.red,
                                  fontSize: 12,
                                ),
                              ),
                            );
                          },
                        ),
                        const SizedBox(height: 30),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          child: ElevatedButton(
                            // ignore: unnecessary_lambdas
                            onPressed: () => store.login(),
                            style: ButtonStyle(
                              alignment: Alignment.center,
                              backgroundColor: MaterialStateProperty.all(
                                ThemeColors.pink,
                              ),
                            ),
                            child: const Text(
                              'ENTRAR',
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Observer(builder: (_) {
              if (store.isLoggingIn) {
                return Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: Colors.black.withOpacity(0.7),
                  child: const Center(
                    child: CircularProgressIndicator(
                      color: ThemeColors.grey,
                      backgroundColor: ThemeColors.pink,
                    ),
                  ),
                );
              }
              return Container();
            }),
          ],
        ),
      );
}
