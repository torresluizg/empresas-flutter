import 'package:empresas_flutter/app/modules/login/domain/entities/credential_params.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/logged_user.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/login/domain/repositories/auth_repository_interfece.dart';
import 'package:empresas_flutter/app/modules/login/infra/repositories/datasources/auth_datasource.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';

class AuthRepository implements IAuthRepository {
  final IAuthDatasource datasource;

  AuthRepository({
    required this.datasource,
  });
  @override
  Future<Either<AuthException, LoggedUser>> login(
          CredentialsParams params) async =>
      // ignore: unnecessary_await_in_return
      await datasource.login(params);
}
