import 'package:empresas_flutter/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:empresas_flutter/app/modules/login/external/dio/dio_post_auth_datasource.dart';
import 'package:empresas_flutter/app/modules/login/infra/repositories/auth_repository.dart';

class AuthFactory {
  LoginUseCase loginUseCaseInstance() => LoginUseCase(
        repository: AuthRepository(
          datasource: DioPostAuthDatasource(),
        ),
      );
}
