import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/credential_params.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:empresas_flutter/app/modules/login/factory/auth_factory.dart';
import 'package:empresas_flutter/app/shared/backend/backend_service.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';
import 'package:empresas_flutter/app/shared/store/user_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStoreBase with _$LoginStore;

abstract class _LoginStoreBase with Store {
  final backendService = Modular.get<BackendService>();

  @observable
  TextEditingController emailController = TextEditingController();
  @observable
  TextEditingController passwordController = TextEditingController();
  @observable
  bool hasText = false;
  @observable
  String errorText = '';
  @observable
  bool isLoggingIn = false;

  @action
  Future<void> login() async {
    isLoggingIn = true;
    errorText = '';
    final CredentialsParams credentials = CredentialsParams(
      email: emailController.text,
      password: passwordController.text,
    );
    final AuthFactory authFactory = AuthFactory();
    final LoginUseCase loginUseCase = authFactory.loginUseCaseInstance();
    final Either<AuthException, LoggedUser> loggedUser =
        await loginUseCase.call(credentials);
    loggedUser.fold(
      (l) {
        if (l.statusCode == 401) {
          errorText = 'Email ou senha incorretos';
        } else {
          errorText = l.message;
        }
      },
      (r) {
        final userStore = Modular.get<UserStore>();
        // ignore: cascade_invocations
        userStore.user = r.client;
        Modular.to.popAndPushNamed('/home');
      },
    );
    isLoggingIn = false;
  }

  void onChanged(String text) {
    if (passwordController.text.isEmpty && emailController.text.isEmpty) {
      hasText = false;
    } else {
      hasText = true;
    }
  }
}
