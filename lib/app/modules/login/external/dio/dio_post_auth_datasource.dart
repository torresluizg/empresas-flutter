import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/credential_params.dart';
import 'package:empresas_flutter/app/modules/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/app/modules/login/infra/repositories/datasources/auth_datasource.dart';
import 'package:empresas_flutter/app/shared/backend/backend_routes.dart';
import 'package:empresas_flutter/app/shared/backend/backend_service.dart';
import 'package:empresas_flutter/app/shared/errors/errors.dart';
import 'package:empresas_flutter/app/shared/models/user_model.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DioPostAuthDatasource implements IAuthDatasource {
  @override
  Future<Either<AuthException, LoggedUser>> login(
      CredentialsParams params) async {
    final backendService = Modular.get<BackendService>();
    final Map<String, String> data = {
      'email': params.email,
      'password': params.password
    };

    try {
      final Response backendResponse = await backendService.io.post(
        '${BackendRoutes.apiVersion}/users/auth/sign_in',
        data: data,
      );
      final LoggedUser loggedUser = LoggedUser(
        client:
            UserModel.fromJson(backendResponse.data as Map<String, dynamic>),
      );
      dioAddCustomHeaders(backendResponse.headers);
      return Right(loggedUser);
    } catch (e) {
      if (backendService.interceptor.errors.isNotEmpty) {
        return Left(
          AuthException(
            message: backendService.interceptor.errors['errorDescription']
                .toString(),
            statusCode: backendService.interceptor.errors['statusCode'] as int,
          ),
        );
      }
      return Left(
        AuthException(
          message: 'Erro interno no login',
        ),
      );
    }
  }

  void dioAddCustomHeaders(Headers header) {
    final backendService = Modular.get<BackendService>();
    // ignore: cascade_invocations
    backendService.addToken(
      header.value('access-token')!,
      header.value('client')!,
      header.value('uid')!,
    );
  }
}
