import 'package:flutter_modular/flutter_modular.dart';
import 'package:empresas_flutter/app/modules/splash/splash_store.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);
  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  final SplashStore store = Modular.get();

  Future<void> redirect() async {
    await Future.delayed(const Duration(seconds: 2)).then(
      (value) => Modular.to.popAndPushNamed('/login'),
    );
  }

  @override
  Widget build(BuildContext context) {
    redirect();
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/splash_bg.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
