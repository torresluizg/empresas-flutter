import 'package:cached_network_image/cached_network_image.dart';
import 'package:empresas_flutter/app/modules/company_detail/company_detail_store.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:empresas_flutter/app/shared/theme/theme_colors.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';

class CompanyDetailPage extends StatefulWidget {
  final CompanyModel company;
  const CompanyDetailPage({required this.company, Key? key}) : super(key: key);
  @override
  CompanyDetailPageState createState() => CompanyDetailPageState();
}

class CompanyDetailPageState extends State<CompanyDetailPage> {
  final CompanyDetailStore store = Modular.get();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Modular.to.pop(),
            icon: const Icon(
              Icons.arrow_back_outlined,
              color: ThemeColors.pink,
            ),
          ),
          title: Text(
            widget.company.enterpriseName!,
            style: const TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        body: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: 180,
              child: CachedNetworkImage(
                imageUrl:
                    'https://empresas.ioasys.com.br/${widget.company.photo!}',
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => const Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.red,
                  ),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    widget.company.description!,
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
