import 'package:empresas_flutter/app/modules/company_detail/company_detail_page.dart';
import 'package:empresas_flutter/app/modules/company_detail/company_detail_store.dart';
import 'package:empresas_flutter/app/shared/models/company_model.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CompanyDetailModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => CompanyDetailStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(
      '/',
      child: (_, args) => CompanyDetailPage(
        company: CompanyModel(),
      ),
    ),
  ];
}
