import 'package:mobx/mobx.dart';

part 'company_detail_store.g.dart';

class CompanyDetailStore = _CompanyDetailStoreBase with _$CompanyDetailStore;

abstract class _CompanyDetailStoreBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
