import 'package:empresas_flutter/app/modules/login/login_module.dart';
import 'package:empresas_flutter/app/modules/splash/splash_module.dart';
import 'package:empresas_flutter/app/shared/backend/backend_service.dart';
import 'package:empresas_flutter/app/shared/store/user_store.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'modules/home/home_module.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => BackendService()),
    Bind.lazySingleton((i) => UserStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(Modular.initialRoute, module: SplashModule()),
    ModuleRoute('/home', module: HomeModule()),
    ModuleRoute('/login', module: LoginModule()),
  ];
}
