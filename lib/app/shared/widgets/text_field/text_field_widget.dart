import 'package:empresas_flutter/app/shared/theme/theme_colors.dart';
import 'package:empresas_flutter/app/shared/widgets/text_field/text_field_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatefulWidget {
  const TextFieldWidget({
    required this.controller,
    required this.title,
    this.key,
    this.hint = 'Null Hint',
    this.maxLines = 1,
    this.expands = false,
    this.icon = const IconData(0),
    this.align,
    this.inputType = TextInputType.text,
    this.isPassword = false,
    this.isSearch = false,
    this.opacity = 0.5,
    this.containerColor = Colors.black,
    this.iconColor = ThemeColors.grey,
    this.textColor = ThemeColors.grey,
    this.onChanged,
    this.callbackSearch,
    this.fontSize = 16,
    this.readyOnly = false,
    this.inputFormatters,
  }) : super(key: key);

  @override
  final Key? key;
  final TextInputFormatter? inputFormatters;
  final TextEditingController controller;
  final String title;
  final String hint;
  final bool expands;
  final int maxLines;
  final Color containerColor;
  final Color iconColor;
  final IconData icon;
  final TextAlign? align;
  final TextInputType inputType;
  final bool isPassword;
  final bool isSearch;
  final double opacity;
  final Color textColor;
  final Function? onChanged;
  final Function? callbackSearch;
  final double fontSize;
  final bool readyOnly;

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  final textFieldStore = TextFieldStore();
  bool _isVisible = false;

  void _onChanged(String value) {
    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }
  }

  void _toggle() {
    if (!mounted) {
      return;
    }
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  Widget build(BuildContext context) => SizedBox(
        height: 60,
        child: TextFormField(
          //validator: validatorFunction,
          inputFormatters:
              widget.inputFormatters != null ? [widget.inputFormatters!] : null,
          textAlignVertical: TextAlignVertical.top,
          keyboardType: widget.inputType,
          obscureText: widget.isPassword ? !_isVisible : false,
          readOnly: widget.readyOnly,
          controller: widget.controller,
          cursorColor: ThemeColors.pink,
          expands: widget.expands,
          maxLines: widget.expands ? null : widget.maxLines,
          style: TextStyle(color: widget.textColor),
          onChanged: (value) => _onChanged(value),
          decoration: InputDecoration(
            alignLabelWithHint: true,
            prefixIcon: widget.isSearch
                ? const Icon(
                    Icons.search_outlined,
                    color: ThemeColors.grey,
                  )
                : null,
            suffixIcon: widget.isPassword
                ? IconButton(
                    icon: Icon(
                      _isVisible
                          ? Icons.visibility_sharp
                          : Icons.visibility_off_sharp,
                      color: ThemeColors.grey,
                    ),
                    // ignore: unnecessary_lambdas
                    onPressed: () => _toggle(),
                  )
                : null,
            labelText: widget.title,
            labelStyle: const TextStyle(
              color: ThemeColors.grey,
            ),
            hintStyle: TextStyle(
              color: ThemeColors.grey.withOpacity(0.4),
            ),
            hintText: widget.hint,
            fillColor: ThemeColors.white,
            filled: true,
            border: const OutlineInputBorder(
              borderSide: BorderSide(
                color: ThemeColors.white,
              ),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: ThemeColors.white,
              ),
            ),
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: ThemeColors.white,
              ),
            ),
            errorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
              color: ThemeColors.red,
            )),
            focusedErrorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
              color: ThemeColors.red,
            )),
          ),
        ),
      );
}
