import 'package:flutter/material.dart';

class ThemeColors {
  static const Color pink = Color(0xFFE01E69);
  static const Color red = Color(0xFFE00000);
  static const Color grey = Color(0xFF666666);
  static const Color white = Color(0xFFF5F5F5);
}
