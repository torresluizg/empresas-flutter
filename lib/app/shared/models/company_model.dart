class CompanyModel {
  int id = -1;
  String? emailEnterprise;
  String? facebook;
  String? twitter;
  String? linkedin;
  String? phone;
  bool ownEnterprise = false;
  String? enterpriseName;
  String? photo;
  String? description;
  String? city;
  String? country;
  int? value;
  double? sharePrice;
  EnterpriseType? enterpriseType;

  CompanyModel({
    this.id = -1,
    this.emailEnterprise,
    this.facebook,
    this.twitter,
    this.linkedin,
    this.phone,
    this.ownEnterprise = false,
    this.enterpriseName,
    this.photo,
    this.description,
    this.city,
    this.country,
    this.value,
    this.sharePrice,
    this.enterpriseType,
  });

  CompanyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    emailEnterprise = json['email_enterprise'] as String?;
    facebook = json['facebook'] as String?;
    twitter = json['twitter'] as String?;
    linkedin = json['linkedin'] as String?;
    phone = json['phone'] as String?;
    ownEnterprise = json['own_enterprise'] as bool;
    enterpriseName = json['enterprise_name'] as String?;
    photo = json['photo'] as String?;
    description = json['description'] as String?;
    city = json['city'] as String?;
    country = json['country'] as String?;
    value = json['value'] as int;
    sharePrice = json['share_price'] as double;
    enterpriseType = json['enterprise_type'] != null
        ? EnterpriseType.fromJson(
            json['enterprise_type'] as Map<String, dynamic>)
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['email_enterprise'] = emailEnterprise;
    data['facebook'] = facebook;
    data['twitter'] = twitter;
    data['linkedin'] = linkedin;
    data['phone'] = phone;
    data['own_enterprise'] = ownEnterprise;
    data['enterprise_name'] = enterpriseName;
    data['photo'] = photo;
    data['description'] = description;
    data['city'] = city;
    data['country'] = country;
    data['value'] = value;
    data['share_price'] = sharePrice;
    if (enterpriseType != null) {
      data['enterprise_type'] = enterpriseType!.toJson();
    }
    return data;
  }
}

class EnterpriseType {
  int id = -1;
  String enterpriseTypeName = 'Null Type Name';

  EnterpriseType({
    this.id = -1,
    this.enterpriseTypeName = 'Null Type Name',
  });

  EnterpriseType.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    enterpriseTypeName = json['enterprise_type_name'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['enterprise_type_name'] = enterpriseTypeName;
    return data;
  }
}
