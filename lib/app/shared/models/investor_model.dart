import 'package:empresas_flutter/app/shared/models/portfolio_model.dart';

class InvestorModel {
  int? id;
  String? investorName;
  String? email;
  String? city;
  String? country;
  double? balance;
  String? photo;
  PortfolioModel? portfolio;
  double? portfolioValue;
  bool? firstAccess;
  bool? superAngel;

  InvestorModel(
      {this.id,
      this.investorName,
      this.email,
      this.city,
      this.country,
      this.balance,
      this.photo,
      this.portfolio,
      this.portfolioValue,
      this.firstAccess,
      this.superAngel});

  InvestorModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] == null ? 0 : json['id'] as int;
    investorName =
        json['investor_name'] == null ? '' : json['investor_name'] as String;
    email = json['email'] == null ? '' : json['email'] as String;
    city = json['city'] == null ? '' : json['city'] as String;
    country = json['country'] == null ? '' : json['country'] as String;
    balance = json['balance'] == null ? 0 : json['balance'] as double;
    photo = json['photo'] == null ? '' : json['photo'] as String;
    portfolio = json['portfolio'] != null
        ? PortfolioModel.fromJson(json['portfolio'] as Map<String, dynamic>)
        : null;
    portfolioValue =
        json['portfolio_value'] == null ? 0 : json['portfolio_value'] as double;
    firstAccess =
        json['first_access'] == null ? false : json['first_access'] as bool;
    superAngel =
        json['super_angel'] == null ? false : json['super_angel'] as bool;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['investor_name'] = investorName;
    data['email'] = email;
    data['city'] = city;
    data['country'] = country;
    data['balance'] = balance;
    data['photo'] = photo;
    if (portfolio != null) {
      data['portfolio'] = portfolio!.toJson();
    }
    data['portfolio_value'] = portfolioValue;
    data['first_access'] = firstAccess;
    data['super_angel'] = superAngel;
    return data;
  }
}
