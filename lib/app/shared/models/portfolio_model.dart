class PortfolioModel {
  int? enterprisesNumber;
  List? enterprises;

  PortfolioModel({this.enterprisesNumber, this.enterprises});

  PortfolioModel.fromJson(Map<String, dynamic> json) {
    enterprisesNumber = json['enterprises_number'] == null
        ? 0
        : json['enterprises_number'] as int;
    if (json['enterprises'] != null) {
      enterprises = [];
      json['enterprises'].forEach((v) {
        enterprises!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['enterprises_number'] = enterprisesNumber;
    if (enterprises != null) {
      data['enterprises'] = enterprises!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
