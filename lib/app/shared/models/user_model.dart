import 'package:empresas_flutter/app/shared/models/investor_model.dart';

class UserModel {
  InvestorModel? investor;
  String? enterprise;
  bool? success;

  UserModel({this.investor, this.enterprise, this.success});

  UserModel.fromJson(Map<String, dynamic> json) {
    investor = json['investor'] != null
        ? InvestorModel.fromJson(json['investor'] as Map<String, dynamic>)
        : null;
    enterprise = json['enterprise'] == null ? '' : json['enterprise'] as String;
    success = json['success'] == null ? false : json['success'] as bool;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (investor != null) {
      data['investor'] = investor!.toJson();
    }
    data['enterprise'] = enterprise;
    data['success'] = success;
    return data;
  }
}
