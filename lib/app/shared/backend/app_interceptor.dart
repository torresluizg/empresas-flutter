import 'package:dio/dio.dart';

class AppInterceptor extends Interceptor {
  Map<String, dynamic> errors = {};
  bool returnLogin = false;

  @override
  void onRequest(
      RequestOptions options,
      // ignore: avoid_renaming_method_parameters
      RequestInterceptorHandler requestInterceptorHandler) {
    super.onRequest(options, requestInterceptorHandler);
  }

  @override
  void onError(
      DioError err,
      // ignore: avoid_renaming_method_parameters
      ErrorInterceptorHandler errorInterceptorHandler) async {
    final int? statusCode = err.response?.statusCode;
    if (statusCode != null) {
      switch (statusCode) {
        case 400:
          errors = {
            'errorDescription': 'Erro 400',
            'statusCode': statusCode,
          };
          break;
        case 401:
          errors = {
            'errorDescription': 'Não autorizado',
            'statusCode': statusCode,
          };
          break;
        case 404:
          errors = {
            'errorDescription': 'Página não encontrada',
            'statusCode': statusCode,
          };
          break;
        case 500:
          errors = {
            'errorDescription': 'Erro interno do servidor',
            'statusCode': statusCode,
          };
          break;
        default:
          errors = {
            'errorDescription': 'Erro interno do servidor',
          };
      }
    } else {
      errors = {
        'errorDescription': 'Erro interno do servidor',
      };
    }

    return super.onError(err, errorInterceptorHandler);
  }

  @override
  void onResponse(
          Response response,
          // ignore: avoid_renaming_method_parameters
          ResponseInterceptorHandler responseInterceptorHandler) =>
      super.onResponse(response, responseInterceptorHandler);
}
