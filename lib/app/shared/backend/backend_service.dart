import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/shared/backend/app_interceptor.dart';
import 'package:empresas_flutter/app/shared/backend/backend_routes.dart';
import 'package:flutter_modular/flutter_modular.dart';

class BackendService extends Disposable {
  BackendService() {
    io.interceptors.add(interceptor);
    io.options.baseUrl = BackendRoutes.devHost;
    io.options.connectTimeout = 15000;
    io.options.receiveTimeout = 15000;
  }

  final io = Dio();
  final interceptor = AppInterceptor();

  void addToken(String token, String client, String uid) {
    io.options.headers = {
      'access-token': token,
      'client': client,
      'uid': uid,
    };
  }

  void deleteToken() {
    io.options.headers.remove('access-token');
    io.options.headers.remove('client');
    io.options.headers.remove('uid');
  }

  @override
  void dispose() {}
}
