import 'package:flutter_modular_test/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:empresas_flutter/app/modules/company_detail/company_detail_module.dart';

void main() {
  setUpAll(() {
    initModule(CompanyDetailModule());
  });
}
