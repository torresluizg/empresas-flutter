import 'package:empresas_flutter/app/modules/company_detail/company_detail_store.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late CompanyDetailStore store;

  setUpAll(() {
    store = CompanyDetailStore();
  });

  test('increment count', () async {
    expect(store.value, equals(0));
    store.increment();
    expect(store.value, equals(1));
  });
}
